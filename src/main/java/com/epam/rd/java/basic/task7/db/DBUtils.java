package com.epam.rd.java.basic.task7.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtils {
    private static final String CONNECTION_URL =
            "jdbc:derby:memory:testdb;create=true";

    public static void close(Connection con) {
        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static Connection getConnection() throws SQLException {
        Connection con = DriverManager.getConnection(CONNECTION_URL);
        // adjust con
//		con.setAutoCommit(false);
//		con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
        return con;

    }

    public static void rollback(Connection con) {
        if (con != null) {
            try {
                con.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
