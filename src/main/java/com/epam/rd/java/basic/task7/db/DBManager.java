package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static final String URL = "jdbc:mysql://localhost:3306/test2db";
	private static final String USERNAME = "root";
	private static final String PASS = "mysql12";

	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if(instance == null){
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
	}

	private static final String SQL_FIND_ALL_USERS =
			"select * from users";
	private static final String SQL_FIND_ALL_TEAMS =
			"select * from teams";
	private static final String SQL_FIND_USER_BY_LOGIN =
			"select * from users where login=?";
	private static final String SQL_FIND_TEAM_BY_NAME =
			"select * from teams where name=?";
	private static final String SQL_INSERT_USER =
			"insert into users values (default, ?)";
	private static final String SQL_INSERT_TEAM =
			"insert into teams values (default, ?)";
	private static final String SQL_UPDATE_TEAM =
			"update teams set name=? where id=?";
	private static final String SQL_DELETE_USER =
			"delete from users where id=?";
	private static final String SQL_DELETE_TEAM =
			"delete from teams where id=?";
	private static final String SQL_INSERT_TEAM_FOR_USER =
			"insert into users_teams values (?, ?)";
	private static final String SQL_GET_USER_TEAMS =
			"select * from teams where id in(select team_id from users_teams where user_id=?)";

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		try (Connection con = DBUtils.getConnection()) {
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(SQL_FIND_ALL_USERS);
			while (rs.next()) {
				users.add(extractUser(rs));
			}
		} catch (SQLException ex) {
			throw new DBException(ex.getMessage(), ex);
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		boolean res = false;

		try (Connection con = DBUtils.getConnection()) {
			PreparedStatement pstmt =
					con.prepareStatement(
							SQL_INSERT_USER,
							Statement.RETURN_GENERATED_KEYS);

			int k = 1;
			pstmt.setString(k++, user.getLogin());

			if (pstmt.executeUpdate() > 0) {
				ResultSet rs = pstmt.getGeneratedKeys();

				if (rs.next()) {
					int userId = rs.getInt(1);
					user.setId(userId);
					res = true;
				}
			}
		} catch (SQLException ex) {
			throw new DBException(ex.getMessage(), ex);
		}
		return res;
	}

	public boolean deleteUsers(User... users) throws DBException {
		boolean res = false;
		Connection con = null;
		try  {
			con = DBUtils.getConnection();
			con.setAutoCommit(false);
			for (User u :
						users) {
				PreparedStatement pstmt =
						con.prepareStatement(SQL_DELETE_USER);
				int k = 1;
				pstmt.setInt(k++, u.getId());
				res = pstmt.executeUpdate() > 0;
			}

			con.commit();
		} catch (SQLException ex) {
			DBUtils.rollback(con);
			throw new DBException(ex.getMessage(), ex);
		}
		return res;
	}

	public User getUser(String login) throws DBException {
		User user = null;
		try (Connection con = DBUtils.getConnection()) {
			PreparedStatement pstmt =
					con.prepareStatement(SQL_FIND_USER_BY_LOGIN);
			pstmt.setString(1, login);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				user = extractUser(rs);
			}
		} catch (SQLException ex) {
			throw new DBException(ex.getMessage(), ex);
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = null;
		try (Connection con = DBUtils.getConnection()) {
			PreparedStatement pstmt =
					con.prepareStatement(SQL_FIND_TEAM_BY_NAME);
			pstmt.setString(1, name);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				team = extractTeam(rs);
			}
		} catch (SQLException ex) {
			throw new DBException(ex.getMessage(), ex);
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		try (Connection con = DBUtils.getConnection()) {
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(SQL_FIND_ALL_TEAMS);
			while (rs.next()) {
				teams.add(extractTeam(rs));
			}
		} catch (SQLException ex) {
			throw new DBException(ex.getMessage(), ex);
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		boolean res = false;

		try (Connection con = DBUtils.getConnection()) {
			PreparedStatement pstmt =
					con.prepareStatement(
							SQL_INSERT_TEAM,
							Statement.RETURN_GENERATED_KEYS);

			int k = 1;
			pstmt.setString(k++, team.getName());

			if (pstmt.executeUpdate() > 0) {
				ResultSet rs = pstmt.getGeneratedKeys();

				if (rs.next()) {
					int teamId = rs.getInt(1);
					team.setId(teamId);
					res = true;
				}
			}
		} catch (SQLException ex) {
			throw new DBException(ex.getMessage(), ex);
		}
		return res;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		boolean res = false;
		Connection con = null;
		try  {
			con = DBUtils.getConnection();
			con.setAutoCommit(false);
			PreparedStatement pstmt =
					con.prepareStatement(
							SQL_INSERT_TEAM_FOR_USER,
							Statement.RETURN_GENERATED_KEYS);

			for (Team t:
				 teams) {
				int k = 1;
				pstmt.setString(k++, String.valueOf(user.getId()));
				pstmt.setString(k++, String.valueOf(t.getId()));
				if (pstmt.executeUpdate() > 0) {
					ResultSet rs = pstmt.getGeneratedKeys();

					if (rs.next()) {
						int teamId = rs.getInt(1);
						res = true;
					}
				}
			}
			con.commit();
		} catch (SQLException ex) {
			DBUtils.rollback(con);
			throw new DBException(ex.getMessage(), ex);
		}
		return res;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		try (Connection con = DBUtils.getConnection()) {
			PreparedStatement pstmt =
					con.prepareStatement(SQL_GET_USER_TEAMS);
			pstmt.setString(1, String.valueOf(user.getId()));
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				teams.add(Team.createTeam(rs.getString(2)));
			}

		} catch (SQLException ex) {
			throw new DBException(ex.getMessage(), ex);
		}
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		boolean res = false;
		try (Connection con = DBUtils.getConnection()) {
			PreparedStatement pstmt =
					con.prepareStatement(SQL_DELETE_TEAM);

			int k = 1;
			pstmt.setInt(k++, team.getId());

			res = pstmt.executeUpdate() > 0;
		} catch (SQLException ex) {
			throw new DBException(ex.getMessage(), ex);
		}
		return res;
	}

	public boolean updateTeam(Team team) throws DBException {
		boolean res = false;
		try (Connection con = DBUtils.getConnection()) {
			PreparedStatement pstmt =
					con.prepareStatement(SQL_UPDATE_TEAM);

			int k = 1;
			pstmt.setString(k++, team.getName());
			pstmt.setInt(k++, team.getId());

			res = pstmt.executeUpdate() > 0;
		} catch (SQLException ex) {
			throw new DBException(ex.getMessage(), ex);
		}
		return res;
	}

	private static User extractUser(ResultSet rs) throws SQLException {
		User user = User.createUser(rs.getString("login"));
		user.setId(rs.getInt("id"));

		return user;
	}

	private static Team extractTeam(ResultSet rs) throws SQLException {
		Team team = Team.createTeam(rs.getString("name"));
		team.setId(rs.getInt("id"));

		return team;
	}

}
